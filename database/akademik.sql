/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : akademik

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 09/12/2020 13:54:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kategori_buku
-- ----------------------------
DROP TABLE IF EXISTS `kategori_buku`;
CREATE TABLE `kategori_buku`  (
  `id_jenis_buku` int NOT NULL AUTO_INCREMENT,
  `jenis_buku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_jenis_buku`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kategori_buku
-- ----------------------------
INSERT INTO `kategori_buku` VALUES (1, 'Buku Teknik Informatika');
INSERT INTO `kategori_buku` VALUES (2, 'Buku Kimia');
INSERT INTO `kategori_buku` VALUES (3, 'Buku Multimedia');
INSERT INTO `kategori_buku` VALUES (4, 'Buku Fisika');
INSERT INTO `kategori_buku` VALUES (5, 'Buku Bahasa Indonesia');

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa`  (
  `id_mahasiswa` int NOT NULL AUTO_INCREMENT,
  `id_jenis_buku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id_buku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nim` char(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `jurusan` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id_mahasiswa`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
INSERT INTO `mahasiswa` VALUES (1, '1', '2', '123', 'andri', 'SI');
INSERT INTO `mahasiswa` VALUES (2, '1', '3', '31211312', 'dede', 'SI');
INSERT INTO `mahasiswa` VALUES (3, '3', '3', '12365756', 'andri', 'TK');

-- ----------------------------
-- Table structure for nama_buku
-- ----------------------------
DROP TABLE IF EXISTS `nama_buku`;
CREATE TABLE `nama_buku`  (
  `id_buku` int NOT NULL AUTO_INCREMENT,
  `id_jenis_buku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `judul_buku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_buku`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of nama_buku
-- ----------------------------
INSERT INTO `nama_buku` VALUES (1, '1', 'Teknik Informatika 1');
INSERT INTO `nama_buku` VALUES (2, '1', 'Teknik Informatika 2');
INSERT INTO `nama_buku` VALUES (3, '1', 'Teknik Informatika 3');
INSERT INTO `nama_buku` VALUES (4, '1', 'Teknik Informatika 4');
INSERT INTO `nama_buku` VALUES (5, '1', ' Teknik Informatika 5');
INSERT INTO `nama_buku` VALUES (6, '2', 'Kimia 1');
INSERT INTO `nama_buku` VALUES (7, '2', 'Kimia 2');
INSERT INTO `nama_buku` VALUES (8, '2', 'Kimia 3');
INSERT INTO `nama_buku` VALUES (9, '2', 'Kimia 4');
INSERT INTO `nama_buku` VALUES (10, '3', 'Multimedia 1');
INSERT INTO `nama_buku` VALUES (11, '3', 'Multimedia 2');
INSERT INTO `nama_buku` VALUES (12, '4', 'Fisika 1');
INSERT INTO `nama_buku` VALUES (13, '4', 'Fisika 2');
INSERT INTO `nama_buku` VALUES (14, '4', 'Fisika 3');
INSERT INTO `nama_buku` VALUES (15, '5', 'Bahasa Indonesia 1');
INSERT INTO `nama_buku` VALUES (16, '5', 'Bahasa Indonesia 2');
INSERT INTO `nama_buku` VALUES (17, '5', 'Bahasa Indonesia 3');

SET FOREIGN_KEY_CHECKS = 1;
