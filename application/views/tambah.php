<form method="post" id="form">
    <div class="form-group">
        <label for="email">NIM:</label>
        <input type="text" class="form-control"  name="nim" placeholder="Masukan NIM">
    </div>
    <div class="form-group">
         <label for="email">Nama:</label>
        <input type="text" class="form-control"  name="nama" placeholder="Masukan nama" >
    </div>
    <div class="form-group">
        <label>Jurusan:</label>
        <select class="form-control" name="jurusan">
            <option value="TI">Teknik Informatika</option>
            <option value="SI">Sistem Informasi</option>
            <option value="TK">Teknik Komputer</option>
            <option value="MI">Manajemen Informatika</option>
        </select>
    </div>
    <div class="form-group">
        <label>Kategori Buku:</label>
        <select class="form-control" name="kategori_buku">
            <option value="1">Buku Teknik Informatika</option>
            <option value="2">Buku Kimia</option>
            <option value="3">Buku Multimedia</option>
            <option value="4">Buku Fisika</option>
            <option value="5">Buku Bahasa Indonesia</option>
        </select>
    </div>
    <div class="form-group">
        <label>Judul Buku:</label>
        <select class="form-control" name="judul_buku">
            <option value="1">Teknik Informatika 1</option>
            <option value="2">Teknik Informatika 2</option>
            <option value="3">Teknik Informatika 3</option>
            <option value="4">Teknik Informatika 4</option>
            <option value="5"> Teknik Informatika 5</option>
            <option value="6">Kimia 1</option>
            <option value="7">Kimia 2</option>
            <option value="8">Kimia 3</option>
            <option value="9">Kimia 4</option>
            <option value="10">Multimedia 1</option>
            <option value="11">Multimedia 2</option>
            <option value="12">Fisika 1</option>
            <option value="13">Fisika 2</option>
            <option value="14">Fisika 3</option>
            <option value="15">Bahasa Indonesia 1</option>
            <option value="16">Bahasa Indonesia 2</option>
            <option value="17">Bahasa Indonesia 3</option>
        </select>
    </div>
    <button id="tombol_tambah" type="button" class="btn btn-primary" data-dismiss="modal" >Tambah</button>
</form>
<script type="text/javascript">
        $(document).ready(function(){
            $("#tombol_tambah").click(function(){
                var data = $('#form').serialize();
                $.ajax({
                    type	: 'POST',
                    url	: "<?php echo base_url(); ?>/mahasiswa/simpanMahasiswa",
                    data: data,
                    cache	: false,
                    success	: function(data){
                        $('#tampil').load("<?php echo base_url(); ?>/mahasiswa/tampilMahasiswa");
                    }
                });
            });
        });
</script> 