<?php
Class Model_mahasiswa extends CI_Model
{
  function TampilMahasiswa() 
    {
      $this->db->select("*");
      $this->db->from("mahasiswa AS a");
      $this->db->join("kategori_buku as b","b.id_jenis_buku = a.id_jenis_buku");
      $this->db->join("nama_buku as c","c.id_buku = a.id_buku");
      return $this->db->get()->result();
    }

    function Getnim($nim = '')
    {
      return $this->db->get_where('mahasiswa', array('nim' => $nim))->row();
    }
    function HapusMahasiswa($nim)
    {
        $this->db->delete('mahasiswa',array('nim' => $nim));
    }
}
?>